package see.schemeonyou.data;

import java.util.Objects;

public class Link {
    private Attribute source;
    private Attribute target;

    @Override
    public String toString() {
        return "Link{" +
                "target=" + target +
                ", source=" + source +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Link)) {
            return false;
        }
        return this.source.equals(((Link)obj).source)
                && this.target.equals(((Link)obj).target);
    }

    @Override
    public int hashCode() {
        return Objects.hash(source, target);
    }
}
