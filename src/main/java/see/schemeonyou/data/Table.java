package see.schemeonyou.data;

import java.util.List;
import see.collection.SeeListUtils;


public class Table {
    private String name;
    private List<Attribute> attributes;
    private List<Link> links;

    public boolean isSameTable(Table table) {
        return this.name.equals(table.name);
    }

    public boolean hasSameAttributes(Table table) {
        if (this.attributes.size() != table.attributes.size()) {
            return false;
        }
        for (Attribute attr : this.attributes) {
            if (SeeListUtils.getFirst(attr, table.attributes) == null) {
                return false;
            }
        }
        return true;
    }
}
