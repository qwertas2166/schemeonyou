package see.schemeonyou.data;

public enum Constraint {
    PRIMARY_KEY,
    FOREIGN_KEY,
    UNIQUE,
    NOT_NULL
}
