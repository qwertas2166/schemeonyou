package see.schemeonyou.data;

import java.util.EnumSet;

public class Attribute {
    private String name;
    private String dataType;
    private EnumSet<Constraint> constraint;
}
