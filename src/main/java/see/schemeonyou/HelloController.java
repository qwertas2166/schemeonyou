package see.schemeonyou;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

public class HelloController {
    @FXML
    private Label welcomeText;

    @FXML
    protected void onHelloButtonClick() {
        VBox vBox = new VBox();
        welcomeText.setText("Welcome to JavaFX Application!");
    }

//    public void addButton(){
//        Button sound_button = new Button("Button_" + index);
//        button_grid.add(sound_button, index,2);
//        index++;
//    }
}