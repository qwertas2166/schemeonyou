package see.schemeonyou.base;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import see.schemeonyou.HelloApplication;

import java.io.IOException;

public abstract class Loader<C extends Controller<?>> {

    public C load() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource(this.getFxmlName()));
        Parent parent = fxmlLoader.load();
        C controller = fxmlLoader.getController();
        this.initialize(controller);
        return controller;
    }

    protected abstract void initialize(C controller);

    protected abstract String getFxmlName();
}
