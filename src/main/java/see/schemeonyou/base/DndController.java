package see.schemeonyou.base;

public abstract class DndController<C extends Controller<? extends Loader<C>>> {


    protected abstract void registerDnd(C controller);
}
