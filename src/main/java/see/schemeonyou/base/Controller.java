package see.schemeonyou.base;

public abstract class Controller<L extends Loader> {

    public void initialize() {
        Loader loader = this.getLoader();
        loader.initialize(this);
    }

    protected abstract L getLoader();
}
