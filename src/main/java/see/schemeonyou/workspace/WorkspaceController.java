package see.schemeonyou.workspace;

import javafx.fxml.FXML;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.Pane;
import see.schemeonyou.base.Controller;

public class WorkspaceController extends Controller<WorkspaceLoader> {

    @FXML
    Pane pane;

    @FXML
    protected void onDragAndDropButtonClick() {
        System.out.println("onDragAndDropButtonClick");
    }

    public void initialize() {
        init();
    }

    @Override
    protected WorkspaceLoader getLoader() {
        return new WorkspaceLoader();
    }

    public void init() {
        this.pane.setOnDragEntered(event -> {
            Dragboard dragboard = event.getDragboard();
            String droppedData = dragboard.getString();
            System.out.println("setOnDragEntered: " + droppedData);
            event.consume();
        });
        this.pane.setOnDragExited(event -> {
            Dragboard dragboard = event.getDragboard();
            String droppedData = dragboard.getString();
            System.out.println("setOnDragExited: " + droppedData);
            event.consume();
        });
        this.pane.setOnDragOver(event -> {
            event.acceptTransferModes(TransferMode.ANY);
            Dragboard dragboard = event.getDragboard();
            String droppedData = dragboard.getString();
            System.out.println("setOnDragOver: " + droppedData);
            event.consume();
        });
        this.pane.setOnDragDropped(event -> {
            Dragboard dragboard = event.getDragboard();
            String droppedData = dragboard.getString();
            System.out.println("setOnDragDropped: " + droppedData);
            event.setDropCompleted(true);
            event.consume();
        });
    }
}
