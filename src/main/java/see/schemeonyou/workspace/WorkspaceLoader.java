package see.schemeonyou.workspace;

import see.schemeonyou.base.Loader;

public class WorkspaceLoader extends Loader<WorkspaceController> {
    public WorkspaceLoader() {
    }

    @Override
    public void initialize(WorkspaceController controller) {
        new WorkspaceDadController().registerDnd(controller);
    }

    @Override
    public String getFxmlName() {
        return "Workspace.fxml";
    }

}
