package see.schemeonyou.workspace;

import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import see.schemeonyou.base.DndController;

public class WorkspaceDadController extends DndController<WorkspaceController> {

    public WorkspaceDadController() {
    }

    @Override
    protected void registerDnd(WorkspaceController workspaceController) {
        workspaceController.pane.setOnDragEntered(event -> {
            Dragboard dragboard = event.getDragboard();
            String droppedData = dragboard.getString();
            System.out.println("setOnDragEntered: " + droppedData);
            event.consume();
        });
        workspaceController.pane.setOnDragExited(event -> {
            Dragboard dragboard = event.getDragboard();
            String droppedData = dragboard.getString();
            System.out.println("setOnDragExited: " + droppedData);
            event.consume();
        });
        workspaceController.pane.setOnDragOver(event -> {
            event.acceptTransferModes(TransferMode.ANY);
            Dragboard dragboard = event.getDragboard();
            String droppedData = dragboard.getString();
            System.out.println("setOnDragOver: " + droppedData);
            event.consume();
        });
        workspaceController.pane.setOnDragDropped(event -> {
            Dragboard dragboard = event.getDragboard();
            String droppedData = dragboard.getString();
            System.out.println("setOnDragDropped: " + droppedData);
            event.setDropCompleted(true);
            event.consume();
        });
    }

}
