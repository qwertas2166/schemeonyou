package see.schemeonyou.tools;

import javafx.application.Platform;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import see.schemeonyou.base.DndController;

public class ToolsDndController extends DndController<ToolsController> {

    public ToolsDndController() {
    }

    @Override
    protected void registerDnd(ToolsController toolsController) {
        toolsController.table.setOnDragDetected(event -> {
            Dragboard dragboard = toolsController.table.startDragAndDrop(TransferMode.ANY);
            ClipboardContent clipboardContent = new ClipboardContent();
            clipboardContent.putString(toolsController.table.getText());
            System.out.println("setOnDragDetected: " + clipboardContent.getString());
            dragboard.setContent(clipboardContent);
            event.consume();
        });
//        this.table.setOnMouseDragged(event -> {
//            if (this.buttonHolder.get() == null) {
//                Button button = this.copy("tmpDraggedButton_" + UUID.randomUUID(), this.table);
//                this.buttonHolder.set(button);
//            }
//            Button button = this.buttonHolder.get();
////            System.out.println("Dragged: " + button.getTranslateX() + " " + button.getTranslateX());
//            button.setTranslateX(event.getScreenX());
//            button.setTranslateY(event.getScreenY());
//            event.consume();
//        });
        toolsController.accordion.setExpandedPane(toolsController.links);
        Platform.runLater(() -> toolsController.accordion.setExpandedPane(toolsController.objects));
        toolsController.accordion.expandedPaneProperty().addListener((obs, oldPane, newPane) -> {
            if (oldPane != null) {
                oldPane.setCollapsible(true);
            }
            if (newPane != null) {
                Platform.runLater(() -> newPane.setCollapsible(false));
            }
        });
    }
}
