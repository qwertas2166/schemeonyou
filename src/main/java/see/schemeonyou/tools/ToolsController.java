package see.schemeonyou.tools;

import javafx.fxml.FXML;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.TitledPane;
import javafx.scene.input.DragEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import see.collection.Holder;
import see.schemeonyou.base.Controller;

public class ToolsController extends Controller<ToolsLoader> {
    @FXML
    TitledPane objects;
    @FXML
    TitledPane links;
    @FXML
    Accordion accordion;
    @FXML
    Button table;
    private final Holder<Button> buttonHolder = new Holder<>();

    public ToolsController() {

    }

    /*
        // Set the setOnDragDetected event handler on the source node
        sourceNode.setOnDragDetected(event -> {
            Dragboard dragboard = sourceNode.startDragAndDrop(TransferMode.ANY);
            ClipboardContent content = new ClipboardContent();
            // Define the dragboard content
            dragboard.setContent(content);
            event.consume();
        });

        // Set the setOnDragDropped event handler on the target node
        targetNode.setOnDragDropped(event -> {
            // Access the dropped data from the dragboard
            Dragboard dragboard = event.getDragboard();
            String droppedData = dragboard.getString();
            // Process the dropped data
            // ...
            event.setDropCompleted(true);
            event.consume();
        });
     */


    @Override
    protected ToolsLoader getLoader() {
        return new ToolsLoader();
    }

    @FXML
    public void tableOnDragDetected() {
        Button button = this.copy("tmpDraggedButton", this.table);
    }

    @FXML
    public void tableOnDragOver(DragEvent event) {
        System.out.println(event.getTransferMode());
    }

    private Button copy(String name, Button button) {
        System.out.println("Creating button");
        Button newButton = new Button(name);
        newButton.setTranslateX(button.getTranslateX() + 20);
        newButton.setTranslateY(button.getTranslateY() + 20);
        newButton.setTranslateZ(button.getTranslateZ());
        newButton.setBackground(button.getBackground());
        newButton.setPrefSize(button.getPrefWidth(), button.getPrefHeight());
        newButton.setText(button.getText());
        newButton.setGraphic(button.getGraphic());
        newButton.setStyle(button.getStyle());
        newButton.setMnemonicParsing(button.isMnemonicParsing());
        newButton.setAccessibleRole(button.getAccessibleRole());
        newButton.setAccessibleText(button.getAccessibleText());
        newButton.setAccessibleHelp(button.getAccessibleHelp());
        newButton.setAccessibleRoleDescription(button.getAccessibleRoleDescription());
        newButton.setVisible(true);
        newButton.setMnemonicParsing(button.isMnemonicParsing());

        ((AnchorPane) this.objects.getContent()).getChildren().stream()
                .filter(node -> node instanceof GridPane)
                .findFirst()
                .ifPresent(node -> ((GridPane) node).getChildren().add(newButton));
        return newButton;
    }

}
