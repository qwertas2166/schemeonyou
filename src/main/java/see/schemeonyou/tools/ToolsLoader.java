package see.schemeonyou.tools;

import see.schemeonyou.base.Loader;

public class ToolsLoader extends Loader<ToolsController> {
    @Override
    protected void initialize(ToolsController toolsController) {
        ToolsDndController toolsDad = new ToolsDndController();
        toolsDad.registerDnd(toolsController);
    }

    @Override
    protected String getFxmlName() {
        return "Tools.fxml";
    }
}
