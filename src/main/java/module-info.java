module see.schemeonyou {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.web;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires net.synedra.validatorfx;
    requires org.kordamp.ikonli.javafx;
//    requires eu.hansolo.tilesfx;
    requires com.almasb.fxgl.all;
    requires static SeeCommon;

    opens see.schemeonyou to javafx.fxml;
    exports see.schemeonyou;
    exports see.schemeonyou.base;
    opens see.schemeonyou.base to javafx.fxml;
    exports see.schemeonyou.workspace;
    opens see.schemeonyou.workspace to javafx.fxml;
    exports see.schemeonyou.tools;
    opens see.schemeonyou.tools to javafx.fxml;
}